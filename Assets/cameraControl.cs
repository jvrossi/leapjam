﻿using UnityEngine;
using System.Collections;

public class cameraControl : MonoBehaviour {
	private HandModel handModel;

	// Use this for initialization
	void Start () {
		handModel = transform.GetComponent<HandModel> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (handModel != null){
			GameObject camera = GameObject.FindGameObjectWithTag ("MainCamera");
			GameObject handController = GameObject.FindGameObjectWithTag ("HandController");

			Vector3 handPosition = handModel.transform.position;
			float distance = Vector3.Distance (camera.transform.position, handPosition);
			Debug.Log (distance);
			if(distance > 5.0f){
				//camera.transform.position += transform.forward * 1.0f * Time.deltaTime;
				//handController.transform.position += transform.forward * 1.0f * Time.deltaTime;
			}
		}
	}
}
